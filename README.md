# vim-syntax-pep484
This adds syntax highlighting for Python PEP484 (type hints).

## How to use
copy python.vim `~/.vim/after/syntax/`.
