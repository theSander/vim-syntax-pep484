syn match fn_return_type_annotation_type ") *->\zs *[a-zA-Z0-9\[\]_\-, ""]\+\ze *: *$"
syn region comment_type_annotation start="# *type: *" keepend end=' *$'
syn match type_annotation_type containedin=comment_type_annotation ":\zs *[a-zA-Z0-9\[\]_\-, ""]\+\ze *\([,)=]\|$\)"

syn keyword Keyword self

hi def link comment_type_annotation PreProc
hi def link type_annotation_keyword PreProc
hi def link type_annotation_type Type
hi def link fn_return_type_annotation_type Type
